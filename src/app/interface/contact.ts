export interface Contact{
    id:number,
    firstname:string,
    lastname:string,
    profilePicture?:string,
    age?:number,
    birthdate?:string
}
