import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ContactModelService } from "../../../services/contact-model.service";
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html'
  
})
export class DeleteComponent implements OnInit {
  id;
  constructor(private model: ContactModelService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => (this.id = params.id));
  }

  deleteContact() {
    this.model.deleteContact(this.id, ()=>{this.router.navigate(['/contacts'])});
}
}
