import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ContactModelService } from "../../../services/contact-model.service";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html'
  
})
export class InfoComponent implements OnInit {
  
  constructor(
    private route: ActivatedRoute,
    private ruter: Router,
    private model: ContactModelService
  ) { }

  contact = {};
  ngOnInit() {

    this.route.parent.params.subscribe(params => {
      
      this.model.getContactId(params.id, a => {
        //console.log(a);
        this.contact = a;
      });
    });
  }

}
