import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IMyDpOptions } from "../../../../../node_modules/angular4-datepicker/src/my-date-picker";
import { ContactModelService } from "../../../services/contact-model.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html"
})
export class EditComponent implements OnInit {
  editContactForm: FormGroup;
  submitedtried = false;
  obj;
  tekucaGodina = new Date().getFullYear();
  editDataPickerOptions: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };
  constructor(
    private route: ActivatedRoute,
    private model: ContactModelService,
    private router: Router
  ) {
    this.editContactForm = new FormGroup({
      id: new FormControl("", Validators.required),
      firstname: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      lastname: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      profilePicture: new FormControl(""),
      age: new FormControl(""),
      birthdate: new FormControl("")
    });
  }

  contact = {};

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.model.getContactId(params.id, a => {
        this.contact = a;
        this.editContactForm.setValue(a);
        let date = new Date(a.birthdate);
        this.editContactForm.patchValue({
          birthdate: {
            date: {
              year: date.getFullYear(),
              month: date.getMonth() + 1,
              day: date.getDate()
            }
          }
        });
        //this.editContactForm.value.birthdate.jsdate=this.contact.birthdate;
      });
    });
  }

  sabmitForm() {
    this.submitedtried = true;
    /* if (this.editContactForm.valid) {
      let obj = {
        ...this.editContactForm.value,
        birthdate: this.editContactForm.value.birthdate.jsdate
      };

      this.model.updateContact(obj, () => {
        this.router.navigate(["/contacts"]);
      });
    } */
    if (
      this.editContactForm.valid &&
      (this.editContactForm.value.birthdate != "" ||
        this.editContactForm.value.age != "")
    ) {
      this.obj = {
        ...this.editContactForm.value,
        birthdate: this.editContactForm.value.birthdate.jsdate
      };
      if (this.editContactForm.value.birthdate != "") {
        if (
          this.tekucaGodina ==
          Number(this.editContactForm.value.age) +
            this.editContactForm.value.birthdate.jsdate.getFullYear()
        ) {
          this.model.updateContact(this.obj, () => {
            this.router.navigate(["/contacts"]);
          });
        } else {
          this.editContactForm.controls.age['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";
          this.editContactForm.controls.birthdate['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";
        }
      } else {
        this.model.updateContact(this.obj, () => {
          this.router.navigate(["/contacts"]);
        });
      }
    } else {
      //alert("Proverite datum rodjenja i godine");
      //this.editContactForm.controls.age['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";

    }
  }
}
