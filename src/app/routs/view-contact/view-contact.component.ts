import { Component, OnInit } from "@angular/core";
import { ContactModelService } from "../../services/contact-model.service";
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';
@Component({
  selector: "app-view-contact",
  templateUrl: "./view-contact.component.html"
})
export class ViewContactComponent implements OnInit {
  constructor(public model: ContactModelService, private router: Router) {}

  openContact(id) {
    this.router.navigate(["/contacts", id]);
  }
  editContact(id) {
    this.router.navigate(["/contacts", id, "edit"]);
  }
  deleteContact(id) {
    this.router.navigate(["/contacts", id, "delete"]);
  }

  ngOnInit() {}
}
