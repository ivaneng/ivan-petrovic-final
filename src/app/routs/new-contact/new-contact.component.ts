import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ContactModelService } from "../../services/contact-model.service";
import { IMyDpOptions } from "../../../../node_modules/angular4-datepicker/src/my-date-picker";
@Component({
  selector: "app-new-contact",
  templateUrl: "./new-contact.component.html"
})
export class NewContactComponent implements OnInit {
  addContactForm: FormGroup;
  submitedtried = false;
  godinaRodjenja;
  tekucaGodina = new Date().getFullYear();
  obj;
  editDataPickerOptions: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(private model: ContactModelService, private router: Router) {
    this.addContactForm = new FormGroup({
      firstname: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),
      lastname: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),
      profilePicture: new FormControl(""),
      age: new FormControl("", [Validators.pattern(/^\d{1,3}$/)]),
      birthdate: new FormControl("")
    });
  }

  ngOnInit() {}

  sabmitForm() {
    this.submitedtried = true;

    if (
      this.addContactForm.valid &&
      (this.addContactForm.value.birthdate != "" ||
        this.addContactForm.value.age != "")
    ) {
      this.obj = {
        ...this.addContactForm.value,
        birthdate: this.addContactForm.value.birthdate.jsdate
      };
      if (this.addContactForm.value.birthdate != "") {
        if (
          this.tekucaGodina ==
          Number(this.addContactForm.value.age) +
            this.addContactForm.value.birthdate.jsdate.getFullYear()
        ) {
          this.model.addContact(this.obj, () => {
            this.router.navigate(["/contacts"]);
          });
        } else {
          this.addContactForm.controls.age['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";
          this.addContactForm.controls.birthdate['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";
        }
      } else {
        this.model.addContact(this.obj, () => {
          this.router.navigate(["/contacts"]);
        });
      }
    } else {
      //alert("Proverite datum rodjenja i godine");
      this.addContactForm.controls.age['nepoklapanje'] = "Broj godina se ne poklapa sa godinom rodjenja!";

    }
  }
}
