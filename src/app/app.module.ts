import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { DeleteComponent } from "./routs/contact/delete/delete.component";
import { EditComponent } from "./routs/contact/edit/edit.component";
import { MyDatePickerModule } from '../../node_modules/angular4-datepicker/src/my-date-picker/my-date-picker.module';
import { HomeComponent } from "./routs/home/home.component";
import { NewContactComponent } from "./routs/new-contact/new-contact.component";
import { ViewContactComponent } from "./routs/view-contact/view-contact.component";
import { InfoComponent } from "./routs/contact/info/info.component";
import { ContactComponent } from "./routs/contact/contact.component";
import { ContactModelService } from "./services/contact-model.service";
import { ContactServiceService } from "./services/contact-service.service";
import { ManuComponent } from "./manu/manu.component";
import { SearchPipe } from "./pipe/search.pipe";
import { DatePipe } from "./pipe/date.pipe";

const routs: Routes = [
  { path: "", component: HomeComponent },
  { path: "contacts/new", component: NewContactComponent },
  {
    path: "contacts/:id",
    component: ContactComponent,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: EditComponent },
      { path: "info", component: InfoComponent },
      { path: "delete", component: DeleteComponent }
    ]
  },
  { path: "contacts", component: ViewContactComponent },
  { path: "**", redirectTo: "/" }
];

@NgModule({
  declarations: [
    AppComponent,
    DeleteComponent,
    EditComponent,
    HomeComponent,
    NewContactComponent,
    ViewContactComponent,
    ContactComponent,
    ManuComponent,
    SearchPipe,
    InfoComponent,
    DatePipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routs),
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    MyDatePickerModule
  ],
  providers: [ContactModelService, ContactServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {}
