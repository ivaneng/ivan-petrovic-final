import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class ContactServiceService {

  constructor(private http:Http) { }

  getContactsObservable() {
    return this.http.get("http://localhost:3000/contacts").map(response => {
      return response.json();
    });
  }
  getOneContactObservable(id) {
    return this.http.get("http://localhost:3000/contacts/" + id).map(response => {
      return response.json();
    });
  }
  addOneContactObservable(contact) {
    return this.http
      .post("http://localhost:3000/contacts", contact)
      .map(response => {
        return response.json();
      });
  }
  updateOneContactObservable(contact) {
    return this.http
      .patch("http://localhost:3000/contacts/" + contact.id, contact)
      .map(response => {
        return response.json();
      });
  }
  deleteOneContactObservable(id) {
    return this.http
      .delete("http://localhost:3000/contacts/" + id)
      .map(response => {
        return response.json();
      });
  }

}
