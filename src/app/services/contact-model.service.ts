import { Injectable } from '@angular/core';
import { Contact } from "../interface/contact";
import { ContactServiceService } from "./contact-service.service";

@Injectable()
export class ContactModelService {

  contacts = [];

  constructor(private service:ContactServiceService) {
      this.refreshContacts();
  }


  refreshContacts() {
    this.service.getContactsObservable().subscribe(contacts => {
      this.contacts = contacts;
    });
  }

  refreshContactsClbk(clbk) {
    this.service.getContactsObservable().subscribe(contacts => {
      this.contacts = contacts;
      clbk();
    });
  }

  deleteContact(id, callbackFunkcija) {
    this.service.deleteOneContactObservable(id).subscribe(() => {
      this.refreshContacts();
      callbackFunkcija();
    });
  }
  addContact(contact, clbk) {
    this.service.addOneContactObservable(contact).subscribe(contact => {
      this.contacts.push(contact);
      clbk();
    });
  }
  updateContact(contact, clbk) {
    this.service.updateOneContactObservable(contact).subscribe(contact => {
      this.refreshContacts();
      clbk();
    });
  }
  getContactId(id, clbk) {
    if (this.contacts && this.contacts.length > 0) {
      for (var i = 0; i < this.contacts.length; i++)
        if (this.contacts[i].id == id) {
          clbk(this.contacts[i]);
        }
    } else {
      this.refreshContactsClbk(() => {
        for (var i = 0; i < this.contacts.length; i++)
          if (this.contacts[i].id == id) {
            clbk(this.contacts[i]);
          }
      });
    }
  }
}
