import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormat'
})
export class DatePipe implements PipeTransform {

  transform(datum: any): any {
    var objekatDatuma = new Date(datum);
    
    var dan = objekatDatuma.getDate();
    var mesec = objekatDatuma.getMonth()+1;
    var danString = String(dan);
    var mesecString = String(mesec);
    if(dan<10){danString='0' + danString;}
    if(mesec<10){mesecString='0' + mesecString;}
    
    return danString + '/'+ mesecString +'/'+ objekatDatuma.getFullYear();
  
  }
}
