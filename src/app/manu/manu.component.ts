import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manu',
  templateUrl: './manu.component.html'
  
})
export class ManuComponent implements OnInit {

  links= [
    {
        naziv:'Home',
        putanja:"/",
        aktivan:false
    },
    {
        naziv:'Kontakti',
        putanja:"/contacts",
        aktivan:false
    },
    {
        naziv:'Novi kontakt',
        putanja:"contacts/new",
        aktivan:false
    }

]

  constructor() { }

  ngOnInit() {
  }

}
